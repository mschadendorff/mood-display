import libs.freenect as freenect
import numpy as np
from ctypes import cdll
import tensorflow as tf
from threading import *
import wx
import cv2
from prediction import Prediction
from predicter_event import PredicterEvent

IMG_HEIGHT = 120
IMG_WIDTH = 160

class ResultEvent(wx.PyEvent):
    def __init__(self, evt_result_id, data):
        wx.PyEvent.__init__(self)
        self.SetEventType(evt_result_id)
        self.data = data
        
        
class GesturePredicter(Thread):
    def __init__(self, notify_window, evt_result_id):
        Thread.__init__(self)
        self.notify_window = notify_window
        self.want_abort = 0
        self.evt_result_id = evt_result_id
                  
        # load formware
        lib = cdll.LoadLibrary('/home/pi/Desktop/mood-display/app/libs/fwLoader.so')
        lib.main()

        tf.compat.v1.enable_eager_execution()
        # Load TFLite model and allocate tensors.
        self.interpreter = tf.lite.Interpreter(model_path='/home/pi/Desktop/mood-display/app/assets/model/tf_model_light_dense512small_more_pictures_lr1.tflite')
        self.interpreter.allocate_tensors()

        # Get input and output tensors.
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()
                  
        self.start()
        
    def run(self):
        try:
            while (self.want_abort == 0):
                depth, timestamp = freenect.sync_get_depth() 
                np.clip(depth, 0, 1023, depth)
                depth >>= 2
                depth = depth.astype(np.uint8)
                depthConv = self.process_image(depth)
                
                self.interpreter.set_tensor(self.input_details[0]['index'], depthConv)

                self.interpreter.invoke()

                output_data = self.interpreter.get_tensor(self.output_details[0]['index'])
                
                depth = cv2.flip(depth, 1)
                predicterEvent = PredicterEvent(0, Prediction(np.argmax(output_data[0])), depth)
                wx.PostEvent(self.notify_window, ResultEvent(self.evt_result_id, predicterEvent))
            
            predicterEvent = PredicterEvent(1, 0, 0)
            wx.PostEvent(self.notify_window, ResultEvent(self.evt_result_id, predicterEvent))
            freenect.sync_stop()
        except RuntimeError:
            print("Runtime Error")
            freenect.sync_stop()
        
    def process_image(self, img):
        img = tf.convert_to_tensor(img, dtype=tf.uint8)
        img = tf.expand_dims(img, -1)
        #img = tf.image.crop_to_bounding_box(img, 120, 80, 360, 480)
        img = tf.image.convert_image_dtype(img, tf.float32)
        img = tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])
        return tf.expand_dims(img, 0)
    
    def abort(self):
        self.want_abort = 1

    





