from threading import *
import time
import wx

class ResultEvent(wx.PyEvent):
    def __init__(self, evt_gauge_id, data):
        wx.PyEvent.__init__(self)
        self.SetEventType(evt_gauge_id)
        self.data = data
        
class TimerThread(Thread):
    def __init__(self, notify_window, evt_timer_id):
        Thread.__init__(self)
        self.notify_window = notify_window
        self.evt_timer_id = evt_timer_id
        
    def run(self):
        i = 0
        while (i < 6):
            time.sleep(1)  
            wx.PostEvent(self.notify_window, ResultEvent(self.evt_timer_id, i))
            i = i + 1