from enum import Enum

class Prediction(Enum):
    NO_ARM = 0
    RIGHT_ARM = 1
    NO_HUMAN = 2
    LEFT_ARM = 3