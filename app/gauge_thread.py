from threading import *
import time
import wx

class ResultEvent(wx.PyEvent):
    def __init__(self, evt_gauge_id, data):
        wx.PyEvent.__init__(self)
        self.SetEventType(evt_gauge_id)
        self.data = data
        
class GaugeThread(Thread):
    def __init__(self, notify_window, evt_gauge_id):
        Thread.__init__(self)
        self.notify_window = notify_window
        self.evt_gauge_id = evt_gauge_id
        self.progress = 0
        self.wantAbort = 0
        
    def run(self):
        while (self.progress <= 61 and self.wantAbort == 0):
            self.progress = self.progress + 1
            wx.PostEvent(self.notify_window, ResultEvent(self.evt_gauge_id, self.progress))
            time.sleep(0.015)
            
        wx.PostEvent(self.notify_window, ResultEvent(self.evt_gauge_id, 0))    
    
    def abort(self):
        self.wantAbort = 1