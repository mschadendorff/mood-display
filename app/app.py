import wx
from predicter import GesturePredicter
from question import Question
from gauge_thread import GaugeThread
from timer_thread import TimerThread
from prediction import Prediction
import time
import matplotlib
from matplotlib.figure import Figure
import faulthandler;
import numpy as np

matplotlib.rcParams['font.size'] = 18.0
faulthandler.enable()

EVT_RESULT_ID = wx.NewId()
EVT_GAUGE_ID = wx.NewId()
EVT_TIMER_ID = wx.NewId()
QUESTIONS = [
    Question("Ist das Thema Bürgerbeteiligung\nfür dich wichtig?", "Nein", "Ja"),
    Question("Wie fandest du unsere Präsentation?", "Fantastisch", "Großartig"),
    Question("Bist du mit dem Auto oder mit dem\nÖPNV zur Veranstaltung gefahren?", "Auto", "ÖPNV"),
    Question("Wohnst du lieber in der Stadt oder\n auf dem Land?", "Stadt", "Land"),
    Question("Wie findest du die Veranstaltung?", "Langweilig", "Interessant"),
    Question("Wie alt bist du?", "Unter 25", "Über 25"),
    Question("Hast du dir viele Gedanken zu deinem\nheutigen Outfit gemacht?", "Nein", "Ja"),
    Question("Wie geht es dir heute?", "Geht so", "Super"),
    Question("Findest du die Hafencity interessant?", "Nein", "Ja"),
    Question("Findest du es sinnvoll, über diesen\nMechanismus an Umfrage teilzunehmen?", "Nein", "Ja")
    ]
        
def OnEraseBackground(evt):
    dc = evt.GetDC()
 
    if not dc:
        dc = wx.ClientDC(self)
        rect = evt.GetEventObject().GetUpdateRegion().GetBox()
        dc.SetClippingRect(rect)
    dc.Clear()
    bmp = wx.Bitmap("/home/pi/Desktop/mood-display/app/assets/background.png")
    dc.DrawBitmap(bmp, 0, 0)

def ConnectEvent(win, func, resultId):
    win.Connect(-1, -1, resultId, func)
    
def DisconnectEvent(win, func, resultId):
    win.Disconnect(-1, -1, resultId, func)

class StartPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id)
        self.font = wx.Font(20, wx.DECORATIVE, wx .NORMAL, wx.BOLD)
        
        self.instruction = wx.StaticText(self, -1, "Willkommen am Mood Board!\nHebe den rechten oder linken Arm,\num Fragen zu beanworten.", pos = (100, 250), size = (568, 300), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.instruction.SetFont(self.font)
        
        self.countdown = wx.StaticText(self, -1, "", pos = (100, 500), size = (568, 300), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.countdown.SetFont(self.font)
        
        self.manLeftHand = wx.StaticBitmap(self, -1, wx.Image("/home/pi/Desktop/mood-display/app/assets/man_left_hand.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap(), (126, 700))
        self.manLeftHand = wx.StaticBitmap(self, -1, wx.Image("/home/pi/Desktop/mood-display/app/assets/man_right_hand.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap(), (409, 700))
        
        self.Bind(wx.EVT_ERASE_BACKGROUND, OnEraseBackground)
        
class StatisticPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id)
        self.parent = parent

        self.font = wx.Font(20, wx.DECORATIVE, wx .NORMAL, wx.BOLD)
        self.question = wx.StaticText(self, -1, "Danke für Deine Teilnahme!", pos = (100, 100), size = (568, 300), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.question.SetFont(self.font)
        self.thanks = wx.StaticBitmap(self, -1, wx.Image("/home/pi/Desktop/mood-display/app/assets/hand_shake.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap(), (248, 200))
        self.answerLable = wx.StaticText(self, -1, "Auswertung aller bisherigen Abstimmungen", pos = (90, 650), size = (100, 50), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.answerLable.SetFont(self.font)
        
        self.pieBitmap = wx.StaticBitmap(self, -1, wx.NullBitmap, pos = (84, 810))
        
        self.countdown = wx.StaticText(self, -1, "", pos = (100, 1273), size = (568, 50), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.countdown.SetFont(self.font)
        
        self.Bind(wx.EVT_ERASE_BACKGROUND, OnEraseBackground)
    
    def UpdatePie(self):
        self.validAnswers = []
        self.validLabels = []
        
        for index, value in enumerate(self.parent.answers[self.parent.currentQuestion]):
            if (value != 0):
                self.validAnswers.append(value)
                self.validLabels.append(QUESTIONS[self.parent.currentQuestion].answers[index])
        
        self.CreatePie(self.validAnswers, self.validLabels)
        
        self.pieBitmap.SetBitmap(wx.Image("/home/pi/Desktop/mood-display/app/assets/pie" + str(self.parent.currentQuestion) + ".png", wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        
    def CreatePie(self, values, labels):
        self.figure = Figure()
        self.figure.patch.set_alpha(0.)
        self.ax = self.figure.add_subplot(111)
        self.ax.pie(values, labels = labels, autopct = '%1.0f%%', labeldistance = 1.2 )
        self.ax.axis('equal')
        self.figure.savefig("/home/pi/Desktop/mood-display/app/assets/pie" + str(self.parent.currentQuestion) + ".png", bbox_inches = 'tight')
        
class AskPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id)
        self.currentGauge = 0
        self.currentAnswer = 0
        
        self.questionFont = wx.Font(20, wx.DECORATIVE, wx .NORMAL, wx.BOLD)
        self.answerFont = wx.Font(20, wx.DECORATIVE, wx .NORMAL, wx.BOLD)
        self.question = wx.StaticText(self, -1, QUESTIONS[parent.currentQuestion].text, pos = (100, 100), size = (568, 300), style = wx.ALIGN_CENTRE_HORIZONTAL)
        self.question.SetFont(self.questionFont)
        self.answerLeft = wx.StaticText(self, -1, QUESTIONS[parent.currentQuestion].answers[0], pos = (150, 335), size = (200, 150))
        self.answerLeft.SetFont(self.answerFont)
        self.answerRight = wx.StaticText(self, -1, QUESTIONS[parent.currentQuestion].answers[1], pos = (418, 335), size = (200, 150), style = wx.ALIGN_RIGHT)
        self.answerRight.SetFont(self.answerFont)
        self.gaugeLeft = wx.Gauge(self, -1, range = 60, pos = (50, 450), size = wx.Size(150, 40))
        self.gaugeRight = wx.Gauge(self, -1, range = 60, pos = (578, 450), size = wx.Size(150, 40))
        self.depth = wx.StaticBitmap(self, -1, wx.NullBitmap, (64, 886), (640,480))
        self.leftHand = wx.StaticBitmap(self, -1, wx.Image("/home/pi/Desktop/mood-display/app/assets/left_hand.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap(), (50, 300))
        self.rightHand = wx.StaticBitmap(self, -1, wx.Image("/home/pi/Desktop/mood-display/app/assets/right_hand.png", wx.BITMAP_TYPE_PNG).ConvertToBitmap(), (648, 300))
        
        self.Bind(wx.EVT_ERASE_BACKGROUND, OnEraseBackground)
                
class MainFrame(wx.Frame):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, 'Mood Display')
        self.currentPrediction = Prediction.NO_ARM
        self.predictionWeight = 0
        self.answers = np.zeros((10, 2), dtype = np.uint)
        self.currentQuestion = 0
        self.answerFontActive = wx.Font(25, wx.DECORATIVE, wx .NORMAL, wx.BOLD)
        self.worker = GesturePredicter(self, EVT_RESULT_ID)
        self.questionTimer = TimerThread(self, EVT_TIMER_ID)
        
        ConnectEvent(self, self.OnResult, EVT_RESULT_ID)
        ConnectEvent(self, self.NextQuestion, EVT_TIMER_ID)
        ConnectEvent(self, self.UpdateProgress, EVT_GAUGE_ID)
        
        flagsExpand = wx.SizerFlags(1)
        flagsExpand.Expand()
        self.sizer = wx.BoxSizer()
        
        self.startPanel = StartPanel(self, wx.ID_ANY)
        self.startPanel.Bind(wx.EVT_RIGHT_DOWN, self.OnKey)
        
        self.statisticPanel = StatisticPanel(self, wx.ID_ANY)
        self.statisticPanel.Bind(wx.EVT_RIGHT_DOWN, self.OnKey)
        self.statisticPanel.Hide()
        
        self.askPanel = AskPanel(self, wx.ID_ANY)
        self.askPanel.Bind(wx.EVT_RIGHT_DOWN, self.OnKey)
        self.askPanel.Hide()
        
        self.sizer.Add(self.startPanel, 1, wx.EXPAND)
        self.sizer.Add(self.askPanel, 1, wx.EXPAND)
        self.sizer.Add(self.statisticPanel, 1, wx.EXPAND)
        self.currentPanel = self.startPanel
        self.SetSizer(self.sizer)
    
    def OnKey(self, event):
        self.worker.abort()
    
    def OnResult(self, msg):
        predictEvent = msg.data
        
        if (predictEvent.abort == 1):
            self.Close()
            return
            
        if (self.currentPanel != self.statisticPanel):
            predictEvent.img = predictEvent.img.reshape((640, 480))
            predictEvent.img = np.stack((predictEvent.img,) * 3, axis = -1)
            self.askPanel.depth.SetBitmap(wx.Image(640, 480, predictEvent.img).ConvertToBitmap())
            print("currentWeigth: " + str(self.predictionWeight))   
            if (predictEvent.output == self.currentPrediction):
                print("currentPrediction: " + str(self.currentPrediction)) 
                if (self.predictionWeight == 2):
                    if (self.currentPrediction != Prediction.NO_HUMAN):
                        if (self.currentPanel == self.startPanel and not self.questionTimer.isAlive()):
                            print("startQuestionTimer")
                            self.currentCountdown = self.startPanel.countdown
                            self.questionTimer = TimerThread(self, EVT_TIMER_ID)
                            self.questionTimer.start()
                        
                        elif (self.currentPanel == self.askPanel):
                            print("askPanel")
                            if (self.currentPrediction == Prediction.RIGHT_ARM): 
                                self.askPanel.currentGauge = self.askPanel.gaugeRight
                                self.askPanel.currentAnswer = self.askPanel.answerRight
                            elif (self.currentPrediction == Prediction.LEFT_ARM):
                                self.askPanel.currentGauge = self.askPanel.gaugeLeft
                                self.askPanel.currentAnswer = self.askPanel.answerLeft
                         
                            if (self.currentPrediction == Prediction.RIGHT_ARM
                                or self.currentPrediction == Prediction.LEFT_ARM):
                                print("startGaugeThread")
                                self.askPanel.currentAnswer.SetFont(self.answerFontActive)
                                self.gaugeThread = GaugeThread(self, EVT_GAUGE_ID)
                                self.gaugeThread.start()
                    
                    else:
                        self.startTime = time.time()    
                
                if (self.currentPrediction == Prediction.NO_HUMAN and self.predictionWeight > 2):
                    if (time.time() - self.startTime >= 60 and self.currentPanel != self.startPanel):
                        self.ChangePanel(self.askPanel, self.startPanel)
                    
                if (self.predictionWeight < 5):
                    self.predictionWeight = self.predictionWeight + 1
                
            elif (predictEvent.output != self.currentPrediction
                  and self.predictionWeight > 0):
                if (self.predictionWeight == 3 and self.currentPanel == self.askPanel):
                    if (self.currentPrediction == Prediction.RIGHT_ARM
                        or self.currentPrediction == Prediction.LEFT_ARM):
                        self.gaugeThread.abort()
                        self.askPanel.currentAnswer.SetFont(self.askPanel.answerFont)              
                    elif (self.currentPrediction == Prediction.NO_HUMAN):
                        self.startTime = None
                        
                self.predictionWeight = self.predictionWeight - 1
                
            elif (predictEvent.output != self.currentPrediction
                  and self.predictionWeight == 0):
                self.currentPrediction = predictEvent.output
                        
    def UpdateProgress(self, msg):
        if (msg.data < 61):
            self.askPanel.currentGauge.SetValue(msg.data)
        elif (msg.data == 61):
            if (self.currentPrediction == Prediction.RIGHT_ARM):
                self.answerIndex = 1
            else:
                self.answerIndex = 0
            self.answers[self.currentQuestion][self.answerIndex] = self.answers[self.currentQuestion][self.answerIndex] + 1
            print(self.answers)
            self.askPanel.Hide()
            self.statisticPanel.Show()
            self.currentPanel = self.statisticPanel
            self.statisticPanel.UpdatePie()
            self.Layout()
            if (self.currentQuestion == len(QUESTIONS) - 1):
                self.currentQuestion = 0
            else:
                self.currentQuestion = self.currentQuestion + 1
            
            self.askPanel.currentAnswer.SetFont(self.askPanel.answerFont)
            self.askPanel.question.SetLabel(QUESTIONS[self.currentQuestion].text)
            self.askPanel.answerLeft.SetLabel(QUESTIONS[self.currentQuestion].answers[0])
            self.askPanel.answerRight.SetLabel(QUESTIONS[self.currentQuestion].answers[1])
            
            self.currentCountdown = self.statisticPanel.countdown
            questionTimer = TimerThread(self, EVT_TIMER_ID)
            questionTimer.start()
    
    def NextQuestion(self, event):
        if (event.data < 5):
            if (self.currentPanel == self.startPanel):    
                self.currentCountdown.SetLabel("Gleich gehts los: " + str(5 - event.data))
            elif (self.currentPanel == self.statisticPanel):
                self.currentCountdown.SetLabel("Gleich gehts weiter: " + str(5 - event.data))
        else:
            self.currentCountdown.SetLabel("")
            self.ChangePanel(self.currentPanel, self.askPanel)
        
    def ChangePanel(self, old, new):
        self.currentPanel = new
        old.Hide()
        new.Show()
        self.Layout()
        self.ResetPrediction()
        
    def ResetPrediction(self):
        self.currentPrediction = Prediction.NO_ARM
        self.predictionWeight = 0
        
class MainApp(wx.App):
    def OnInit(self):
        self.frame = MainFrame(None, -1)
        self.frame.Show(True)
        self.frame.ShowFullScreen(True)
        self.SetTopWindow(self.frame)
        return True

if (__name__ == '__main__'):
    app = MainApp(0)
    app.MainLoop()
