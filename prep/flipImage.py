import cv2
import os

folder_left = './img/left_arm/'
folder_right = './img/right_arm/'
right_arm = 'right_arm'

file_handle = open(folder_right + 'counter.txt', 'r')
line = file_handle.readline()
file_handle.close()
counter = int(line)

for filename in os.listdir(folder_left):
    if filename.endswith('.jpg'):
        img = cv2.imread(os.path.join(folder_left, filename))
        if img is not None:
            flipVertical = cv2.flip(img, 1)
            #cv2.imshow('image', flipVertical)
            cv2.imwrite(folder_right + right_arm + str(counter) + '.jpg', flipVertical)
            counter = counter + 1

file_handle = open(folder_right + '/counter.txt', 'w')
file_handle.write(str(counter))
file_handle.close()
