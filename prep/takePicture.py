import freenect 
import cv2 
import numpy as np
import time

from ctypes import cdll
lib = cdll.LoadLibrary('./fwLoader.so')

folder = 'no_human'

file_handle = open('./img_cliped_bg/' + folder + '/counter.txt', 'r')
line = file_handle.readline()
counter = int(line)
begin_counter = counter
file_handle.close()

# load formware
lib.main()
time.sleep(1)
    
while True:

    depth, timestamp = freenect.sync_get_depth() 
    np.clip(depth, 0, 1023, depth)
    depth >>= 2
    depth = depth.astype(np.uint8)
    
    #blur = cv2.GaussianBlur(depth, (5, 5), 0) 
    cv2.imshow('image', depth)
    cv2.imwrite('./img_cliped_bg/' + folder + '/' + folder + str(counter) + '.jpg', depth)

    if cv2.waitKey(10) == 27 or counter == begin_counter + 445: 
        break
    time.sleep(0.3)
    counter = counter + 1
    
file_handle = open('./img_cliped_bg/' + folder + '/counter.txt', 'w')
file_handle.write(str(counter))
file_handle.close()
freenect.sync_stop()
cv2.destroyAllWindows()


