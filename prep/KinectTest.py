import freenect 
import cv2 
import numpy as np
from ctypes import cdll
import tensorflow as tf

tf.compat.v1.enable_eager_execution()
IMG_HEIGHT = 120
IMG_WIDTH = 160

def process_image(img):
    # Transformieren von dem Pixelarray in TF Objekt
    img = tf.convert_to_tensor(img, dtype=tf.uint8)
    
    # Tensor an die von vom Neuronalen Netz verstandene Struktur anpassen
    img = tf.expand_dims(img, -1)
    img = tf.image.convert_image_dtype(img, tf.float32)
    img = tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])
    
    return tf.expand_dims(img, 0)

# Firmware des KinectSensors laden
lib = cdll.LoadLibrary('./fwLoader.so')
lib.main()

class_names = ['no_arm', 'right_arm', 'no_human', 'left_arm']

# TFLite Model laden und Speicherplatz allozieren
interpreter = tf.lite.Interpreter(model_path='/home/pi/Desktop/mood-display/prep/tf_model_light_dense512small_more_pictures_lr1.tflite')
interpreter.allocate_tensors()

# Referenzen auf den allozierten Speicherplatz
# für Eingabe und Ausgabetensoren bekommen
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
    
while True:
    # Sensorbild bekommen (in Graustufen)
    depth, timestamp = freenect.sync_get_depth()
    
    # Pixel des Sensorbilds auf 8 Bit Werte verkleinern
    np.clip(depth, 0, 1023, depth)
    depth >>= 2
    depth = depth.astype(np.uint8)
    
    # Bild verarbeiten
    depthConv = process_image(depth)
     
    # Eingabetensor in allozierten Speicherplatz kopieren
    interpreter.set_tensor(input_details[0]['index'], depthConv)
    
    # Inferenz starten
    interpreter.invoke()

    # Kopie des Ausgabetensor bekommen
    # Beispielausgabe: [0.001234, 0.015869, 0.095978, 0.886919]
    output_data = interpreter.get_tensor(output_details[0]['index'])
    
    # Bild für Ausgabe vergrößern
    depth = cv2.resize(depth, (IMG_WIDTH*7, IMG_HEIGHT*7))
        
    # Anzeigen der Klasse mit der höchsten Wahrscheinlichkeit
    font = cv2.FONT_HERSHEY_SIMPLEX 
    thickness = 1
    image = cv2.putText(depth,
                        'Mit ' + str(int(np.amax(output_data[0]) * 100)) + '% Wahrscheinlichkeit: ' + class_names[np.argmax(output_data[0])],
                        (400,50), font, 0.5, (0, 0, 0) , thickness, cv2.LINE_AA)
    
    # Anzeigen des Bildes
    cv2.imshow('Gestenerkennung', depth)
    
    # Warten auf Escape Taste
    if cv2.waitKey(10) == 27: 
        break
 
# Aufräumen
cv2.destroyAllWindows()
freenect.sync_stop()
#img = tf.image.crop_to_bounding_box(img, 120, 80, 360, 480)

