import tensorflow as tf

converter = tf.lite.TFLiteConverter.from_saved_model('./../res/tf_model_dense512small_test')
tflite_model = converter.convert()

open('./../res/model_light/presentation/tf_model_light.tflite', 'wb').write(tflite_model)
