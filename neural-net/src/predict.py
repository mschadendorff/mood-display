import tensorflow as tf
import pathlib
import numpy as np
data_dir = './../img'
data_dir = pathlib.Path(data_dir)
model = tf.keras.models.load_model('./../res/tf_model')

def decode_img(img):
    # convert the compressed string to a 3D uint8 tensor
    img = tf.image.decode_jpeg(img, channels=1)
    # Use `convert_image_dtype` to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # resize the image to the desired size.
    return tf.image.resize(img, [240, 320])


def process_path(file_path):
    # load the raw data from the file as a string
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    return img

img = process_path(tf.Variable('./../temp/right_arm/right_arm2.jpg', dtype=tf.string))
img = tf.expand_dims(img, 0)
print(model.predict(img))
