from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
import pathlib

AUTOTUNE = tf.data.experimental.AUTOTUNE


def process_path(file_path):
    # Zugehöriges Label finden
    label = get_label(file_path)
    # Bild laded
    img = tf.io.read_file(file_path)
    img = decode_img(img)

    return img, label


def get_label(file_path):
    # Pfad in seine Komponenten zerlegen
    parts = tf.strings.split(file_path, os.path.sep)
    # Die vorletzte Komponente ist der Ordnername
    # Gibt ein boolean Array, z.B.: [false, false, false, true, false, false]
    return parts[-2] == CLASS_NAMES


def decode_img(img):
    # Konvertieren des Strings in ein 3D uint Tensorobjekt
    img = tf.image.decode_jpeg(img, channels=1)
    # Konvertieren in floats im Intervall von 0 bis 1
    img = tf.image.convert_image_dtype(img, tf.float32)
    # Größe anpassen
    return tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])


def prepare_for_training(ds, cache=True, shuffle_buffer_size=2100):
    # 'cache' lässt den Datensatz im Arbeitsspeicher
    # Falls der Datensatz zu groß ist werden vorbereitende Arbeiten in einer Datei gecached
    if cache:
        if isinstance(cache, str):
            ds = ds.cache(cache)
        else:
            ds = ds.cache()

    # Datensatz wird gemischt
    ds = ds.shuffle(buffer_size=shuffle_buffer_size)

    # Falls alle Daten
    ds = ds.repeat()

    # Batchgröße festlegen
    ds = ds.batch(BATCH_SIZE)

    # `prefetch` lässt neue Batches während des Trainings im Hintergrund laden
    ds = ds.prefetch(buffer_size=AUTOTUNE)

    return ds


# laden der Bilddaten
data_dir = './../img'
data_dir = pathlib.Path(data_dir)

# Datensatz aus allen Pfaden zu den Beispieldaten
list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*.jpg'))

CLASS_NAMES = np.array([item.name for item in data_dir.glob('*') if item.name != ".DS_Store"])
print(CLASS_NAMES)

# Konstanten setzen
DATASET_SIZE = len(list(data_dir.glob('*/*.jpg')))
CLASS_COUNT = CLASS_NAMES.size
BATCH_SIZE = 32
IMG_HEIGHT = 120
IMG_WIDTH = 160
TRAIN_SHARE = 0.9
VALIDATION_SHARE = 1 - TRAIN_SHARE
STEPS_PER_EPOCH = np.ceil(DATASET_SIZE * TRAIN_SHARE / BATCH_SIZE)
VALIDATION_STEPS = np.ceil(DATASET_SIZE * VALIDATION_SHARE / BATCH_SIZE)
EPOCHS = 20

# Setzen von `num_parallel_calls` damit mehrere Bilder parallel verarbeitet werden
labeled_ds = list_ds.map(process_path, num_parallel_calls=AUTOTUNE)

# Aus den Pfaden die Bilder und zugehörige Label machen
full_ds = prepare_for_training(labeled_ds)

# Trainingsdatensatz und Validierungsdatensatz erstellen
train_size = int(TRAIN_SHARE * DATASET_SIZE)
train_ds = full_ds.take(train_size)
val_ds = full_ds.skip(train_size)


# Definition der Schichten des Neuronalen Netzes
model = Sequential([
    Conv2D(16, 3, padding='same', activation='relu',
           input_shape=(IMG_HEIGHT, IMG_WIDTH, 1)),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2D(32, 3, padding='same', activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2D(64, 3, padding='same', activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Flatten(),
    Dropout(0.2),
    Dense(32, activation='relu'),
    Dropout(0.2),
    Dense(CLASS_COUNT, activation='softmax')
])

# Abnahme der Lernrate
initial_learning_rate = 0.001
lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate,
    decay_steps=800,
    decay_rate=0.95,
    staircase=False)

# Angabe der Fehlerfunktion und der Optimierungsfunktion
model.compile(loss='categorical_crossentropy',
              optimizer=tf.keras.optimizers.Adam(learning_rate=lr_schedule),
              metrics=['accuracy'])


# Erstellt einen callback der all 5 Epochen die Gewichte des Modells speichert
checkpoint_path = './../res/tf_model_presentation/checkpoints/cp-{epoch:03d}.ckpt'
cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path,
    verbose=1,
    save_weights_only=True)

# Festlegen der Anzahl der Epoche, Batches pro Epoche
# Training starten
history = model.fit(
    train_ds,
    validation_data=val_ds,
    validation_steps=VALIDATION_STEPS,
    epochs=EPOCHS,
    steps_per_epoch=STEPS_PER_EPOCH,
    callbacks=[cp_callback]
)


# Erzeugtes Modell speichern
tf.saved_model.save(model, './../res/tf_model_presentation')

# Graph von Genauigkeit und Fehlerfunktion anzeigen
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()













